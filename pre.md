版权声明：  
特别顾问：  
策划：    
编辑：  
支持单位：Gitee

# 前言

## 背景
编程语言是软件业的工业母机、编译器技术是信息产业的根技术，各种编程语言被用于操作系统、数据库管理系统、网络服务、工控设备、应用程序等的开发，渗透到了所有现代产业和服务领域。尤其是信息产业创新空间的持续扩展、系统复杂度的持续上升、开发成本的持续降低，都直接受益于不断涌现的编程语言和编译技术。迄今为止，国内几乎没有出现被广泛使用的编程语言，这与我国世界性工业大国、科技大国的地位相去甚远。

工业和信息化部发布的《“十四五”软件和信息技术服务业发展规划》中提到，应“强化基础组件供给……加快突破编程语言开发框架”；中国软件行业协会发布的《中国软件根技术发展白皮书（基础软件册）》第四章专门对编程语言和编译器的重要性、发展态势等进行了归纳。这些文件说明编程语言相关产业的发展获得了政策支持。信息技术在我国经过多年发展积累，已形成从业人数近千万的大型产业，对编程语言这一基本工具的需求本就非常强烈；而大语言模型、国产芯片等新兴方向的井喷式增长更是对编程语言提出了很多全新的需求。

回顾历史不难发现，与其他产业不同，作为信息产业的核心，编程语言的成功案例充满了偶然性。目前广泛使用的编程语言和开发工具，既有由大型企业推动的商业项目，也有由个人发起的开源项目；既有以KPI为驱动的商业产品，也有由兴趣驱动的产品。当前国内的根软件行业也正呈现出项目高度分散的趋势，企业、开源社区发起了大量不同类型、用于不同领域的新兴编程语言项目。

基于上述背景，编程语言开放社区（PLOC）编写了《国产编程语言蓝皮书-2023》（即本文，以下简称蓝皮书），力争全面的收纳国内已具备一定可用性的、活跃的编程语言项目，为业界提供一份客观的国产语言全景图。

## 收录标准

符合以下条件的项目可在蓝皮书工作区仓库中通过PR发起申报：  
1.	项目由国内的企业、社区或个人发起和维护  
2.	项目符合项目分类标准（见后）  
3.	项目满足基础可用且能够被编委会独立验证具备可验证的可用性  
4.	面向公众开放  
5.	项目处于活动状态  

蓝皮书工作区仓库地址：https://gitee.com/ploc-org/CNPL-2023
> 《国产编程语言蓝皮书-2023》编委会对上述标准拥有最终解释权。

## 收录方法

蓝皮书中收录的项目均为自主申报，满足收录标准的项目方可在 https://gitee.com/ploc-org/CNPL-2023/tree/master/projects 目录中增加项目同名目录，将项目简介等资料以 markdown 格式填入其中，填写要求及案例见：https://gitee.com/ploc-org/CNPL-2023/tree/master/projects/sample 。
发起申报 PR 后，编委会将审核项目资料，期间请保持项目地址及网站等可正常访问、联系方式可用；编委会委员将与您联系，确认项目资料准确无误，若您对于某些选项该如何填写存在疑问，亦可在此时与编委沟通。当您发起申报时，视同您已获得该项目所有者许可，并授权编程语言开放社区（PLOC）在蓝皮书中展示该项目的名称、图标等信息。

## 项目分类方法
语言类项目分类标签：
- 付费/免费
- 开源/闭源
- 通用/专用
- 是否接受社区贡献
- 语言类别（详细清单见 附录一语言类别列表）
- 工具类别（详细清单见 附录二工具类别列表）
- 应用领域（详细清单见 附录三应用领域列表）

工具类项目分类标签：
- 付费/免费
- 开源/闭源
- 是否接受社区贡献
- 工具类别（详细清单见 附录二工具类别列表）
- 应用领域（详细清单见 附录三应用领域列表）


# 项目列表（Todo）


# 附录

## 附录一语言类别列表
a. 一般编程语言（General Programming Languages）  
b. 并行语言（Parallel Programming Languages）  
c. 并发语言（Concurrent Programming Languages）  
d. 分布式语言（Distributed Programming Languages）  
e. 命令式语言（Imperative Languages）  
f. 面向对象语言（Object Oriented Languages）  
g. 函数式语言（Functional Languages）  
h. 约束和逻辑语言（Constraint and Logic Languages）  
i. 数据流语言（Data Flow Languages）  
j. 可扩展语言（Extensible Languages）  
k. 汇编语言（Assembly Languages）  
l. 多范式语言（Multiparadigm Languages）  
m. 高级编程语言（Very High Level Language）  

## 附录二工具类别列表
a. 一般编译工具（General Compilers）  
b. 解释器（Interpreters）  
c. 增量编译器（Incremental Compilers）  
d. 可重定向编译器（Retargetable Compilers）  
e. 实时编译器（Just-in-time Compilers）  
f. 动态编译器（Dynamic Compilers）  
g. 生成器（Translator Writing Systems and Compiler Generators）  
h. 代码生成（Source Code Generation）  
i. 运行时环境（Runtime Environment）  
j. 预处理器（Preprocessors）  
k. 解析器（Parsers）  

## 附录三应用领域列表
a. 通用（General Computation）  
b. 计算理论（Theory of Computation）  
c. 计算数学（Mathematics of Computing）  
d. 网路（Network）  
e. 信息系统（Information Systems）  
f. 安全（Security）  
g. 机器学习（Machine Learning）  
h. 人工智能（Artificial Intelligence）  
i. 并行计算（Parallel Computing）  
j. 并发计算（Concurrent Computing）  
k. 分布式计算（Distributed Computing）  
l. 建模与模拟（Modeling and Simulation）  
m. 计算机图形（Computer Graphics）  
n. 行业应用（Applied Computing）  
